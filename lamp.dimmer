{
  "title": "lamp.dimmer",
  "description": "Simple dimmable lamp",
  "lang": "en",
  "documentation": "http://recherche.imt-atlantique.fr/xaal/documentation/",
  "ref": "http://recherche.imt-atlantique.fr/xaal/documentation/lamp.dimmer",
  "license": "Copyright Christophe Lohr IMT Atlantique 2020 - Copying and distribution of this file, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved. This file is offered as-is, without any warranty.",
  "extends": "lamp.basic",
  "attributes": { "brightness":"brightness_t", "white_temperature":"white_temperature_t" },
  "methods": {
    "get_attributes": {
      "description": "Return attributes of the device",
      "in": { "attributes":"attributes_t" },
      "out": { "light":"light_t", "brightness":"brightness_t", "white_temperature":"white_temperature_t" }
    },
    "turn_on": {
      "description": "Switch on the lamp",
      "in": { "smooth":"smooth_t" },
      "related_attributes": [ "light" ]
    },
    "turn_off": {
      "description": "Switch off the lamp",
      "in": { "smooth":"smooth_t" },
      "related_attributes": [ "light" ]
    },
    "set_brightness": {
      "description": "Change the brightness of the lamp",
      "in": { "brightness":"brightness_t", "smooth":"smooth_t" },
      "related_attributes": [ "brightness" ]
    },
    "set_white_temperature": {
      "description": "Change the temperature of the white",
      "in": { "white_temperature":"white_temperature_t" },
      "related_attributes": [ "white_temperature" ]
    }
  },
  "notifications": {
    "attributes_change": {
      "description": "Report attributes that have changed",
      "out": { "light":"light_t", "brightness":"brightness_t", "white_temperature":"white_temperature_t" }
    }
  },
  "datamodel": {
    "brightness_t": {
      "description": "Level of the brightness",
      "unit": "%",
      "type": "data = (0..100) / (0.0..100.0)"
    },
    "white_temperature_t": {
      "description": "Temperature of the white",
      "unit": "K",
      "type": "data = 1000..10000"
    },
    "smooth_t": {
      "description": "Delay for a smooth change, in milliseconds",
      "unit": "ms",
      "type": "data = uint"
    },
    "attributes_t": {
      "description": "List of wanted attributes",
      "type": "data = [ ? \"light\", ? \"brightness\", ? \"white_temperature\" ]"
    }
  }
}
