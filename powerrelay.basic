{
  "title": "powerrelay.basic",
  "description": "Simple power relay device",
  "lang": "en",
  "documentation": "http://recherche.imt-atlantique.fr/xaal/documentation/",
  "ref": "http://recherche.imt-atlantique.fr/xaal/documentation/powerrelay.basic",
  "license": "Copyright Christophe Lohr IMT Atlantique 2019 - Copying and distribution of this file, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved. This file is offered as-is, without any warranty.",
  "extends": "basic.basic",
  "attributes": { "power":"power_t" },
  "methods": {
    "get_attributes": {
      "description": "Return attributes of the device, i.e. 'power'",
      "in": { "attributes":"attributes_t" },
      "out": { "power":"power_t" }
    },
    "turn_on": {
      "description": "Switch on the relay",
      "related_attributes": [ "power" ]
    },
    "turn_off": {
      "description": "Switch off the relay",
      "related_attributes": [ "power" ]
    }
  },
  "notifications": {
    "attributes_change": {
      "description": "Report attributes that have changed, i.e. 'power'",
      "out": { "power":"power_t" }
    }
  },
  "datamodel": {
    "power_t": {
      "description": "State of the relay (true=on false=off)",
      "type": "data = bool"
    },
    "attributes_t": {
      "description": "List of wanted attributes",
      "type": "data = [ ? \"power\" ]"
    }
  }
}
