{
  "title": "door.basic",
  "description": "Simple door device",
  "lang": "en",
  "documentation": "http://recherche.imt-atlantique.fr/xaal/documentation/",
  "ref": "http://recherche.imt-atlantique.fr/xaal/documentation/door.basic",
  "license": "Copyright Christophe Lohr IMT Atlantique 2019 - Copying and distribution of this file, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved. This file is offered as-is, without any warranty.",
  "extends": "basic.basic",
  "attributes": { "position":"position_t" },
  "methods": {
    "get_attributes": {
      "description": "Return attributes of the device, i.e. 'position'",
      "in": { "attributes":"attributes_t" },
      "out": { "position":"position_t" }
    },
    "open": {
      "description": "Open the door",
      "related_attributes": [ "position" ]
    },
    "close": {
      "description": "Close the door",
      "related_attributes": [ "position" ]
    }
  },
  "notifications": {
    "attributes_change": {
      "description": "Report attributes that have changed, i.e. 'position'",
      "out": { "position":"position_t" }
    }
  },
  "datamodel": {
    "position_t": {
      "description": "Position of the door (true=open false=close)",
      "type": "data = bool"
    },
    "attributes_t": {
      "description": "List of wanted attributes",
      "type": "data = [ ? \"position\" ]"
    }
  }
}
