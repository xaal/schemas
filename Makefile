SCHEMAS = basic.basic hmi.basic \
	metadatadb.basic cache.basic \
	gateway.basic \
	scenario.basic \
	lamp.basic lamp.dimmer lamp.color lamp.toggle \
	powerrelay.basic powerrelay.toggle \
	switch.basic button.basic button.remote \
	powermeter.basic linkquality.basic battery.basic \
	door.basic window.basic \
	shutter.basic shutter.position worktop.basic \
	thermometer.basic hygrometer.basic barometer.basic co2meter.basic \
	luxmeter.basic lightgauge.basic soundmeter.basic \
	raingauge.basic windgauge.basic \
	scale.basic \
	motion.basic \
	contact.basic \
	falldetector.basic \
	tts.basic

test:
	@$(foreach i,$(SCHEMAS),./validate_schema -s $(i);)

clean:
	-rm -f *~

.PHONY: test clean
	