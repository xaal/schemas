; Definition of Schemas for xAAL version 0.7
; Copyright Christophe Lohr IMT Atlantique 2019
; Copying and distribution of this file, with or without modification, are
; permitted in any medium without royalty provided the copyright notice
; and this notice are preserved.  This file is offered as-is, without any
; warranty.


schema = {
  ; The name of the device schema, i.e. the dev_type
  title: dev_type,

  ; A short description in natural language
  description: tstr,

  ; IETF BCB47 language tag of descriptions
  lang: tstr,

  ; URI (rfc3986) pointing to a more comprehensive documentation
  documentation: tstr,

  ; URI (rfc3986) pointing to the original version of this schema
  ; i.e. before any extention process
  ref: tstr,

  ; License of the the original schema file itself
  ? license: tstr,

  ; The schema name which is extended by this one
  ? extends: dev_type,

  ; List of attributes managed by the device
  ? attributes: { + identifier => type_name },

  ; Methods supported by the device
  ? methods: { + identifier => method },

  ; Notifications emitted by the device
  ? notifications: { + identifier => notification },

  ; Specifications of data mentionned in the schema
  ; Typically: attributes, parameters of methods and notifications
  ? datamodel: { + identifier => datadef }
}



; Format of the name of a schema in the form "foo.bar"
dev_type = tstr .regexp "[a-zA-Z][a-zA-Z0-9_-]*\\.[a-zA-Z][a-zA-Z0-9_-]*"


; Format of names for attributes, methods and notification
identifier = tstr .regexp "[a-zA-Z][a-zA-Z0-9_-]*"

type_name = identifier


; Definition of a method
method = {
  ; A short description in natural language
  description: tstr,

  ; List of input parameters
  ? in: { * identifier => type_name },

  ; List of output data
  ? out: { * identifier => type_name },

  ; List of device attributes that may be modified while invoking the method
  ? related_attributes: [ * identifier ]
}


; Definition of a notification
notification = {
  ; A short description in natural language
  description: tstr,

  ; List of output data
  out: { * identifier => type_name }
}


; Definition of a data
; Used by device attributes, methods and notifications parameters
datadef = {
  ; A short description in natural language
  description: tstr,

  ; Unit, according to the IANA "Sensor Measurement Lists (SenML)" registry
  ? unit: tstr,

  ; Formal descrtiption in CDDL (rfc8610)
  type: tstr
}
